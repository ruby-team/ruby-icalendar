ruby-icalendar (2.10.3-1) unstable; urgency=medium

  * New upstream version 2.10.3
  * Bump Standards-Version to 4.7.0 (no changes needed)
  * Refresh patches

 -- Pirate Praveen <praveen@debian.org>  Thu, 14 Nov 2024 01:27:30 +0530

ruby-icalendar (2.10.0-1) unstable; urgency=medium

  * New upstream version 2.10.0
    + ruby3.2 support
  * Drop X?-Ruby-Versions: fields
  * refresh remove-git-in-gemspec.patch
  * Bump Standards-Version to 4.6.2 (no changes needed)

 -- Cédric Boutillier <boutil@debian.org>  Thu, 02 Nov 2023 15:34:06 +0100

ruby-icalendar (2.8.0-1) unstable; urgency=medium

  * New upstream version 2.8.0
  * Bump Standards-Version to 4.6.1 (no changes needed)

 -- Cédric Boutillier <boutil@debian.org>  Mon, 18 Jul 2022 22:46:41 +0200

ruby-icalendar (2.7.1-1) unstable; urgency=medium

  * Team upload

  [ Utkarsh Gupta ]
  * Add salsa-ci.yml

  [ Debian Janitor ]
  * debian/copyright: use spaces rather than tabs to start continuation
    lines.
  * Bump debhelper from old 11 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Apply multi-arch hints.
    + ruby-icalendar: Add :all qualifier for ruby dependency.
  * Update watch file format version to 4.
  * Bump debhelper from old 12 to 13.

  [ Cédric Boutillier ]
  * New upstream version 2.7.1
    + continue supporting tzinfo 0.x (Closes: #1002105)
  * Refresh packaging files using dh-make-ruby -w
    + use gem install layout
    + Bump standards-version to 4.6.0 (no changes needed)
  * Drop tzinfo-fix.patch, applied upstream
  * Refresh patches
  * rework copyright file for independent license paragraphs

 -- Cédric Boutillier <boutil@debian.org>  Wed, 26 Jan 2022 15:52:17 +0100

ruby-icalendar (2.4.1-2) unstable; urgency=medium

  * Fix test that failed with tzdata >= 2018f (Closes: #912148)

 -- Pirate Praveen <praveen@debian.org>  Wed, 28 Nov 2018 21:22:59 +0530

ruby-icalendar (2.4.1-1) unstable; urgency=medium

  * Initial release (Closes: #902975)

 -- Pirate Praveen <praveen@debian.org>  Wed, 04 Jul 2018 15:42:50 +0530
